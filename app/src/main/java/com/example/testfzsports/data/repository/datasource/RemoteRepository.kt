package com.example.testfzsports.data.repository.datasource

import com.example.testfzsports.data.datasource.RemoteDataSource
import com.example.testfzsports.ui.login.DeviceInfo

class RemoteRepository(private val remoteDataSource: RemoteDataSource) {

    suspend fun getLogin(deviceInfo: String) = remoteDataSource.getLogin(deviceInfo)

    suspend fun getGames(token: String) = remoteDataSource.getGames(token)

    suspend fun getGameDetails(token: String, id: String) = remoteDataSource.getGameDetails(token, id)

}
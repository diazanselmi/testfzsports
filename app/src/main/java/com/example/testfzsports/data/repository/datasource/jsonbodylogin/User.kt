package com.example.testfzsports.data.repository.datasource.jsonbodylogin

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("_id") val id: String,
    val profile: Profile,
    val rbac: Rbac
)
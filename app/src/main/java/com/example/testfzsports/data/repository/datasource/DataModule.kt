package com.example.testfzsports.data.repository.datasource

import com.example.testfzsports.data.datasource.LocalDataSource
import com.example.testfzsports.data.datasource.RemoteDataSource
import com.example.testfzsports.data.repository.datasource.RemoteRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    fun remoteRepositoryProvider(remoteDataSource: RemoteDataSource): RemoteRepository {
        return RemoteRepository( remoteDataSource)
    }

    @Provides
    fun saveUserDataRepositoryProvider(localDataSource: LocalDataSource): SaveUserDataRepository {
        return SaveUserDataRepository(localDataSource)
    }
}
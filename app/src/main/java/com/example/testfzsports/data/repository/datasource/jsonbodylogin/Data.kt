package com.example.testfzsports.data.repository.datasource.jsonbodylogin

data class Data(
    val accessToken: String,
    val expiresIn: String,
    val refreshToken: String,
    val tokenType: String,
    val user: User
)
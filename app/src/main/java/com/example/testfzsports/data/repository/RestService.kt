package com.example.testfzsports.data.repository

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestService {
    fun getRetrofit(): Retrofit {

        return Retrofit.Builder()
            .baseUrl("https://fxservicesstaging.nunchee.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}
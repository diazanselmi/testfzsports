package com.example.testfzsports.data.repository.datasource.jsonbodylogin

data class Profile(
    val countriesLogin: List<CountriesLoginProfile>,
    val countryLogin: CountryLoginProfile,
    val language: String
)
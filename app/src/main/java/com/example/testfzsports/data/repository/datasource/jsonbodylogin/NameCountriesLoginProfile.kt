package com.example.testfzsports.data.repository.datasource.jsonbodylogin

import com.google.gson.annotations.SerializedName

data class NameCountriesLoginProfile(
    val de: String?,
    val en: String?,
    val es: String?,
    val fr: String?,
    val ja: String?,
    val original: String?,
    @SerializedName("pt-BR") val ptBR: String?,
    val ru: String?,
    @SerializedName("zh-CN") val zhCN: String?,
    val zh: String?,
    val it: String?,
    val pt: String?
)
package com.example.testfzsports.data.repository.datasource.jsonbodylogin

data class CountryLoginProfile(
    val cityProfile: CityCountryLoginProfile,
    val code: String,
    val date: String,
    val name: NameCountryLoginProfile
)
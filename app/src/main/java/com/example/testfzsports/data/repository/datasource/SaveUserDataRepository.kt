package com.example.testfzsports.data.repository.datasource

import android.os.Bundle
import com.example.testfzsports.data.datasource.LocalDataSource

class SaveUserDataRepository(private val localDataSource: LocalDataSource) {

    suspend fun saveUserData(email: String,
                                password: String,
                                token: String) = localDataSource.saveUserData(email, password, token)

    suspend fun getToken() = localDataSource.getToken()

}
package com.example.testfzsports.data.repository.datasource.jsonbodylogin

import com.google.gson.annotations.SerializedName

data class NameCityCountryLoginProfile(
    val de: String,
    val en: String,
    val es: String,
    val fr: String,
    val ja: String,
    val original: String,
    @SerializedName("pt-BR") val ptBR: String,
    val ru: String
)
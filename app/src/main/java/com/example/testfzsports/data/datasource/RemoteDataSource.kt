package com.example.testfzsports.data.datasource

import android.os.Bundle
import com.example.testfzsports.ui.login.DeviceInfo

interface RemoteDataSource {
    suspend fun getLogin(deviceInfo: String): Bundle

    suspend fun getGames(token: String): Bundle

    suspend fun getGameDetails(token: String, id: String): Bundle
}
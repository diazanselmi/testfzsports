package com.example.testfzsports.data.repository.datasource.jsonbodylogin

data class CountriesLoginProfile(
    val city: CityCountriesLoginProfile?,
    val code: String?,
    val name: NameCountriesLoginProfile?
)
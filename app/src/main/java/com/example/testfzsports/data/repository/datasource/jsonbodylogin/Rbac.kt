package com.example.testfzsports.data.repository.datasource.jsonbodylogin

data class Rbac(
    val role: String,
    val template: String
)
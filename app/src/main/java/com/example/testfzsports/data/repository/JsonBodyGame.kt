package com.example.testfzsports.data.repository

data class JsonBodyGame(
    val data: Data
){
    data class Data(
        val sections: List<Sections>,
        val items: List<Items>,
        val pagination: Pagination
    )

    data class Sections(
        val index: Int,
        val _id: String,
        val title: Title
    ){
        data class Title(
            val original: String
        )
    }

    data class Items(
        val _id: String,
        val __belong: Belong,
        val _externalProvider: String,
        val _externalId: String,
        val matchDay: MatchDay,
        val eventStatus: EventStatus,
        val statusCategory: String,
        val location: Location,
        val startDate: String,
        val cellType: Int,
        val type: String,
        val homeTeam: HomeTeam,
        val awayTeam: AwayTeam,
        val homeScore: Int,
        val awayScore: Int,
        val matchTime: Int,
        val homePenalties: String,
        val awayPenalties: String,
        val endDate: String,
        val statusDate: String,
        val _createDate: String,
        val __children: Children,
        val _model: String
    ){
        data class Belong(
            val Client: String,
            val AccessGroup: List<String>,
            val Tournament: List<String>
        )

        data class MatchDay(
            val start: String,
            val end: String,
            val name: Name,
            val phase: Phase
        ){
            data class Name(
                val original: String
            )
            data class Phase(
                val original: String
            )
        }

        data class EventStatus(
            val _id: String,
            val name: Name,
            val category: String
        ){
            data class Name(
                val es: String,
                val original: String
            )
        }

        data class Location(
            val original: String
        )

        data class HomeTeam (
            val _id: String,
            val name: String,
            val shortName: String
        )

        data class AwayTeam(
            val _id: String,
            val name: String,
            val shortName: String
        )

        data class Children (
            val TimeLine: List<String>,
            val Formation: List<String>
            )

    }

    data class Pagination (
        val page: Int,
        val first: Int,
        val items: Int,
        val last: Int,
        val next: Int,
        val pages: Int,
        val offset: Int,
        val totalItems: Int
                )
}
package com.example.testfzsports.data.repository.datasource.jsonbodylogin

import com.google.gson.annotations.SerializedName

data class JsonBodyLogin(
    val data: Data
)
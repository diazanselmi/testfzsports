package com.example.testfzsports.data.datasource

import android.os.Bundle

interface LocalDataSource {
    suspend fun saveUserData(email:String, password: String, token: String)

    suspend fun getToken(): String?
}
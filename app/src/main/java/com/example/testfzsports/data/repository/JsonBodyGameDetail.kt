package com.example.testfzsports.data.repository

import com.google.gson.annotations.SerializedName

data class JsonBodyGameDetail(
    val data: Data
){
    data class Data(
        val availableWidgets: List<String>,
        val location: Location,
        val startDate: String,
        val homeScore: Int,
        val awayScore: Int,
        val tournament: Tournament,
        val timeline: List<Timeline>,
        val homeTeam: HomeTeam,
        val awayTeam: AwayTeam,
        val stats: List<Stats>,
        val homeFormation: HomeFormation,
        val awayFormation: AwayFormation,
        val videos: List<String?>,
        val programmingData: ProgrammingData?,
        val wilsonMetadata: WilsonMetadata?,
        @SerializedName("__belong") val belong: Belong,
        val eventStatus: EventStatus,
        val statusCategory: String
    ){
        data class Location(
            val original: String
        )

        data class Tournament(
            @SerializedName("_id") val id: String,
            @SerializedName("_externalId") val externalId: String,
            val name: Name,
            val shortName: ShortName,
            @SerializedName("__belong") val belong: Belong,
            @SerializedName("_externalProvider") val externalProvider: String,
            @SerializedName("_createDate") val createDate: String,
            @SerializedName("__v") val v: Int,
            @SerializedName("__children") val children: Children,
            @SerializedName("_updateDate") val updateDate: String
            ) {
            data class Name(
                val original: String
            )

            data class ShortName(
                val original: String
            )

            data class Belong(
                @SerializedName("Client") val client: String,
                @SerializedName("AccessGroup") val accessGroup: List<String>,

            )

            data class Children(
                @SerializedName("Season") val season: List<String>
            )
        }

        data class Timeline(
            @SerializedName("_id") val id: String,
            @SerializedName("__belong") val belong: Belong,
            @SerializedName("_externalProvider") val externalProvider: String,
            @SerializedName("_externalId") val externalId: String,
            val type: Type,
            val typeKey: String?,
            val isHomeTeam: Boolean,
            val time: Int?,
            val partialResult: PartialResult,
            @SerializedName("_createDate") val createDate: String
        ) {
                data class Belong(
                    @SerializedName("Client") val client: String,
                    @SerializedName("AccessGroup") val accessGroup: List<String>,
                    @SerializedName("Tournament") val tournament: List<String>,
                    @SerializedName("Team") val team: List<Team>,
                    @SerializedName("Player") val player: List<Player>,



                ) {
                    data class Team(
                        @SerializedName("_id") val id: String,
                        val name: String,
                        val shortName: String
                    )

                    data class Player(
                        @SerializedName("_id") val id: String,
                        val name: String
                    )


                }

                data class Type(
                    @SerializedName("_id") val id: String,
                    val description: Description,
                    val icon: String
                ) {
                    data class Description(
                    val original:String,
                    val en: String
                    )
                }

                data class PartialResult(
                    val homeScore: Int?,
                    val awayScore: Int?,
                    val homePenaltyScore: Int?,
                    val awayPenaltyScore: Int?
                )

        }

        data class HomeTeam(
            @SerializedName("_id") val id: String,
            @SerializedName("_externalProvider") val externalProvider: String,
            @SerializedName("_externalId") val externalId: String,
            val name: String,
            val shortName: String,
            val country: Country,
            @SerializedName("_createDate") val createDate: String

        ){
            data class Country(
                val code: String,
                val name: Name
            ){
                data class Name(
                    val original: String,
                    val es: String,
                    val pt: String,
                    val zh: String,
                    val it: String
                )
            }
        }

        data class AwayTeam(
            @SerializedName("_id") val id: String,
            @SerializedName("_externalProvider") val externalProvider: String,
            @SerializedName("_externalId") val externalId: String,
            val name: String,
            val shortName: String,
            val country: Country,
            @SerializedName("_createDate") val createDate: String
        ){
            data class Country(
                val code: String,
                val name: Name
            ){
                data class Name(
                    val original: String,
                    val es: String,
                    val pt: String,
                    val zh: String,
                    val it: String
                )
            }
        }

        data class Stats(
            val title: Title,
            val format: Format,
            val priority: Int,
            val homeValue: Int,
            val awayValue: Int,
            val homeColor: String,
            val awayColor: String
        ){
            data class Title(
                val original: String,
                val en: String
            )

            data class Format(
                val original: String
            )
        }

        data class HomeFormation(
            val starters: List<Starters>,
            val bench: List<Bench>

        ){
            data class Starters(
                val position: Position,
                val number: Int,
                val player: Player,
                val captain: Boolean
            ){
                data class Position(
                    @SerializedName("_id") val id: String,
                    val title: Title,
                    val icon: String
                ){
                    data class Title(
                        val original: String,
                        val en: String
                    )
                }

                data class Player(
                    @SerializedName("_id") val id: String,
                    val name: String,
                    val image: String
                )
            }

            data class Bench(
                val position: Position,
                val number: Int,
                val player: Player,
                val captain: Boolean
            ){
                data class Position(
                    @SerializedName("_id") val id: String,
                    val title: Title,
                    val icon: String
                ){
                    data class Title(
                        val original: String,
                        val en: String
                    )
                }

                data class Player(
                    @SerializedName("_id") val id: String,
                    val name: String,
                    val image: String
                )
            }

        }
        data class AwayFormation(
            val starters: List<Starters>,
            val bench: List<Bench>
        ){
            data class Starters(
                val position: Position,
                val number: Int,
                val player: Player,
                val captain: Boolean
            ){
                data class Position(
                    @SerializedName("_id") val id: String,
                    val title: Title,
                    val icon: String
                ){
                    data class Title(
                        val original: String,
                        val en: String
                    )
                }

                data class Player(
                    @SerializedName("_id") val id: String,
                    val name: String,
                    val image: String
                )
            }

            data class Bench(
                val position: Position,
                val number: Int,
                val player: Player,
                val captain: Boolean
            ){
                data class Position(
                    @SerializedName("_id") val id: String,
                    val title: Title,
                    val icon: String
                ){
                    data class Title(
                        val original: String,
                        val en: String
                    )
                }

                data class Player(
                    @SerializedName("_id") val id: String,
                    val name: String,
                    val image: String
                )
            }
        }

        data class ProgrammingData(
            val id: String?
        )

        data class WilsonMetadata(
            val id: String?
        )

        data class Belong(
                @SerializedName("Client") val client: String,
                @SerializedName("AccessGroup") val accessGroup: List<String>,
                @SerializedName("Tournament") val tournament: List<String>
        )

        data class EventStatus(
            @SerializedName("_id") val id: String,
            val name: Name,
            val category: String
        ){
            data class Name(
                val es: String,
                val original: String
            )
        }

    }
}
package com.example.testfzsports.data.repository

data class JsonBodyLoginRequest(
    val user: User,
    val device: Device,
    val app: App
) {
    data class User(
        val profile: Profile
    ) {
        data class Profile(
            var language: String
        )
    }

    data class Device(
        var deviceId: String,
        var name: String,
        var version: String,
        var width : String,
        var height: String,
        var model: String,
        var platForm: String
    )

    data class App(
        var version:String
    )
}

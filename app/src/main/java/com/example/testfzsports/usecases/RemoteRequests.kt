package com.example.testfzsports.usecases

import android.os.Bundle
import com.example.testfzsports.data.repository.datasource.RemoteRepository
import com.example.testfzsports.ui.login.DeviceInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RemoteRequests(private val remotePostsRepository: RemoteRepository) {

    suspend fun invokeLogin(deviceInfo: String): Bundle = withContext(Dispatchers.IO) {
        remotePostsRepository.getLogin(deviceInfo)
    }

    suspend fun invokeGames(token: String): Bundle = withContext(Dispatchers.IO) {
        remotePostsRepository.getGames(token)
    }

    suspend fun invokeGameDetails(token: String, id: String): Bundle = withContext(Dispatchers.IO) {
        remotePostsRepository.getGameDetails(token, id)
    }

}
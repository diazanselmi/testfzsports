package com.example.testfzsports.usecases

import android.os.Bundle
import android.provider.ContactsContract
import com.example.testfzsports.data.repository.datasource.SaveUserDataRepository
import com.example.testfzsports.ui.login.DeviceInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.PasswordAuthentication

class LocalRequests(private val saveUserDataRepository: SaveUserDataRepository) {
    suspend fun invokeSaveUserData(email: String, password: String,
                                    token: String) = withContext(Dispatchers.IO) {
        saveUserDataRepository.saveUserData(email, password, token)
    }

    suspend fun invokeToken()   = withContext(Dispatchers.IO) {
        saveUserDataRepository.getToken()
    }
}
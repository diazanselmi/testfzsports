package com.example.testfzsports.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestFzSportsApplication: Application()
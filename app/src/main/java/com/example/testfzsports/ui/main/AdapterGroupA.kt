package com.example.testfzsports.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testfzsports.R
import com.example.testfzsports.data.repository.JsonBodyGame

class AdapterGroupA(private val listener: (JsonBodyGame.Items) -> Unit): RecyclerView.Adapter<GroupAViewHolder>() {
    var groupA: List<JsonBodyGame.Items> = emptyList()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupAViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return GroupAViewHolder(layoutInflater.inflate(R.layout.item_games, parent, false))

    }

    override fun onBindViewHolder(holder: GroupAViewHolder, position: Int) {
        val item = groupA[position]
        holder.bind(item)
        holder.itemView.setOnClickListener { listener(item) }

    }

    override fun getItemCount(): Int =  groupA.size


}
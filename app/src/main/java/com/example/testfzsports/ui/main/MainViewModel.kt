package com.example.testfzsports.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testfzsports.data.repository.JsonBodyGame
import com.example.testfzsports.data.repository.datasource.RemoteRepository
import com.example.testfzsports.ui.data.datasource.ServerDataSource
import com.example.testfzsports.usecases.LocalRequests
import com.example.testfzsports.usecases.RemoteRequests
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
        private val remoteRequests: RemoteRequests,
        private  val localRequests: LocalRequests): ViewModel() {

        //private val remoteRequests = RemoteRequests(RemoteRepository(ServerDataSource()))
        
        private val _gameListGroupA = MutableLiveData<List<JsonBodyGame.Items>>()
            val gameListGroupA : LiveData<List<JsonBodyGame.Items>> get() = _gameListGroupA

        private val _gameListGroupB = MutableLiveData<List<JsonBodyGame.Items>>()
            val gameListGroupB : LiveData<List<JsonBodyGame.Items>> get() = _gameListGroupB

        private var _token: String? = String()



        fun getGames() {
                viewModelScope.launch {
                        _token = localRequests.invokeToken()
                        val bundleResponse = _token?.let { remoteRequests.invokeGames(it) }
                        val stringResponse = bundleResponse?.getString("games")
                        setGames(stringResponse)
                }

        }
        private fun setGames(stringResponse: String?) {
                val jsonResponse = Gson().fromJson(stringResponse, JsonBodyGame::class.java)
                val itemListGroupA = mutableListOf<JsonBodyGame.Items>()
                val itemListGroupB = mutableListOf<JsonBodyGame.Items>()
                for (element in jsonResponse.data.items) {
                        if (element.matchDay.name.original.contains("Grupo A", ignoreCase = true)) {
                                itemListGroupA.add(element)
                        } else {
                                itemListGroupB.add(element)
                        }

                }
                _gameListGroupA.value = itemListGroupA
                _gameListGroupB.value = itemListGroupB
        }

        fun onGameClick(game: JsonBodyGame.Items) {
                viewModelScope.launch {
                        val id = game._id
                        val bundleResponseDetail =
                                _token?.let { remoteRequests.invokeGameDetails(it, id) }
                        val stringResponseDetail = bundleResponseDetail?.getString("gameDetail")
                        println(stringResponseDetail)
                }
        }


}
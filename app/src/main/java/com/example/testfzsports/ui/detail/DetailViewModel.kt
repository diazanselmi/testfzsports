package com.example.testfzsports.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testfzsports.data.repository.JsonBodyGameDetail
import com.example.testfzsports.data.repository.datasource.RemoteRepository
import com.example.testfzsports.ui.data.datasource.ServerDataSource
import com.example.testfzsports.usecases.LocalRequests
import com.example.testfzsports.usecases.RemoteRequests
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val remoteRequests: RemoteRequests,
    private val localRequests: LocalRequests     ): ViewModel() {

    private val _gameDetail = MutableLiveData<JsonBodyGameDetail>()
        val gameDetail : LiveData<JsonBodyGameDetail> get() = _gameDetail

    fun getGameDetail(id: String){
        viewModelScope.launch {
            val token = localRequests.invokeToken()
            val bundleResponseDetail = token?.let { remoteRequests.invokeGameDetails(it, id) }
            val stringResponseDetail = bundleResponseDetail?.getString("gameDetail")
            _gameDetail.value = stringResponseDetail?.let { setJsonObjet(it) }

        }
    }
    private fun setJsonObjet(stringResponseDetail: String): JsonBodyGameDetail {
         return Gson().fromJson(stringResponseDetail, JsonBodyGameDetail::class.java)
    }
}
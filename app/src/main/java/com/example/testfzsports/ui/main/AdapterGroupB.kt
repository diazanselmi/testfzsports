package com.example.testfzsports.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testfzsports.R
import com.example.testfzsports.data.repository.JsonBodyGame

class AdapterGroupB(private val listener: (JsonBodyGame.Items) -> Unit): RecyclerView.Adapter<GroupBViewHolder>()  {
    var groupB: List<JsonBodyGame.Items> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupBViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return GroupBViewHolder(layoutInflater.inflate(R.layout.item_games, parent, false))
    }

    override fun onBindViewHolder(holder: GroupBViewHolder, position: Int) {
        val item = groupB[position]
        holder.bind(item)
        holder.itemView.setOnClickListener { listener(item) }
    }

    override fun getItemCount(): Int = groupB.size
}
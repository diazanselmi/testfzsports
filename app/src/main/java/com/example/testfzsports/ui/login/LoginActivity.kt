package com.example.testfzsports.ui.login

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.testfzsports.R
import com.example.testfzsports.databinding.ActivityLoginBinding
import com.example.testfzsports.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var binding: ActivityLoginBinding
    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.progressBarLoggin.visibility = View.GONE

        loginViewModel.isLoginSuccessful.observe(this) {
            if (it.getBoolean("isSuccessful")) {
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("token", it.getString("message"))
                startActivity(intent)
            }
        }


        loginViewModel.loginForm.observe(this) {
            if (!it.isDataValid) {
                Toast.makeText(this, getString(it.invalidFieldError.toInt()), Toast.LENGTH_LONG).show()
                binding.progressBarLoggin.visibility = View.GONE
            }
        }

        binding.button.setOnClickListener {
            binding.progressBarLoggin.visibility = View.VISIBLE
            loginViewModel.loginDataChanged(
                binding.tvUser.editText?.text.toString().trim(),
                binding.tvPassword.editText?.text.toString().trim()
            )
            closeKeyboard()
        }
        getDeviceInfo()
    }

    //hide keyboard after clicked button
    private fun closeKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    //device info for login json body
    private fun getDeviceInfo() {

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        val width = displayMetrics.widthPixels.toString()
        val height = displayMetrics.heightPixels.toString()

        var appVersion = ""
        try {
            appVersion = applicationContext.packageManager
                .getPackageInfo(applicationContext.packageName, 0).versionName.toString()
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("PackageManager", "error: $e")
        }
        val buildVersion = Build.VERSION.RELEASE.toString()
        val deviceName = Build.MODEL.toString()
        val id = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID).toString()
        val model = Build.MANUFACTURER.toString()
        val stringDeviceInfo = "{\n" +
                "\"user\": {\n" +
                "\"profile\": {\n" +
                "\"language\": \"es\"\n" +
                "}\n" +
                "},\n" +
                "\"device\": {\n" +
                "\"deviceId\": \"$id\", \n" +
                "\"name\": \"$deviceName\",\n" +
                "\"version\": \"$buildVersion\",\n" +
                "\"width\": \"$width\",\n" +
                "\"height\": \"$height\",\n" +
                "\"model\": \"$model\",\n" +
                "\"platform\": \"android\"\n" +
                "},\n" +
                "\"app\": {\n" +
                "\"version\": \"1.0.0\"\n" +
                "}\n" +
                "}"
        loginViewModel.setDeviceInfo(stringDeviceInfo)

    }

}


package com.example.testfzsports.ui.data.datasource

import android.os.Bundle
import com.example.testfzsports.data.datasource.RemoteDataSource
import com.example.testfzsports.data.repository.APIService
import com.example.testfzsports.data.repository.RestService
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class ServerDataSource: RemoteDataSource {


    override suspend fun getLogin(deviceInfo: String): Bundle {
        val bundle = Bundle()
        val body = JSONObject(deviceInfo)
        val url = "api/1.0/auth/users/login/anonymous"
        val requestBody = body.toString().toRequestBody("application/json".toMediaTypeOrNull())
        val call = RestService().getRetrofit().create(APIService::class.java).login(url, requestBody)
        return if  (call.isSuccessful){
            bundle.putBoolean("isSuccessful", true)
            bundle.putString("token", call.body()?.data?.accessToken)
            bundle
        } else {
            bundle.putBoolean("isSuccessful", false)
            bundle.putString("message", "Login error, try again!")
            bundle
        }
    }

    override suspend fun getGames(token: String): Bundle {
        val bundle = Bundle()
        val url = "api/1.0/sport/events"
        val header = "Bearer $token"
        val call = RestService().getRetrofit().create(APIService::class.java).getMatches(header, url)
        return if  (call.isSuccessful) {
            val body = call.body()
            val bodyString = Gson().toJson(body)
            bundle.putBoolean("isSuccessful", true)
            bundle.putString("games", bodyString)
            bundle
        } else {
            bundle.putBoolean("isSuccessful", false)
            bundle.putString("games", null)
            bundle
        }
    }



    override suspend fun getGameDetails(token: String, id: String): Bundle {
        val bundle = Bundle()
        val url = "api/1.0/sport/events/$id/details"
        val header = "Bearer $token"
        val call = RestService().getRetrofit().create(APIService::class.java).getGameDetails(header, url)
        return if  (call.isSuccessful) {
            val body = call.body()
            val bodyString = Gson().toJson(body)
            bundle.putBoolean("isSuccessful", true)
            bundle.putString("gameDetail", bodyString)
            bundle
        } else {
            bundle.putBoolean("isSuccessful", false)
            bundle.putString("gameDetail", null)
            bundle
        }
    }


}
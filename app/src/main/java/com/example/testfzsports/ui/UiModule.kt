package com.example.testfzsports.ui

import android.app.Application
import com.example.testfzsports.data.datasource.LocalDataSource
import com.example.testfzsports.data.datasource.RemoteDataSource
import com.example.testfzsports.ui.data.datasource.LocalUserData
import com.example.testfzsports.ui.data.datasource.ServerDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class UiModule {

    @Provides
    fun remoteDataSourceProvider() : RemoteDataSource {
        return ServerDataSource()
    }

    @Provides
    fun localDataSource(app: Application): LocalDataSource {
        val context = app.applicationContext
        return LocalUserData(context)
    }
}
package com.example.testfzsports.ui.data.datasource

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import com.example.testfzsports.data.datasource.LocalDataSource

class LocalUserData(private val context: Context): LocalDataSource {


    override suspend fun saveUserData(email: String, password: String, token: String) {
        val sharedPreferences = context.getSharedPreferences("userData", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply {
            putString("email", email)
            putString("password", password)
            putString("token", token)
        }.apply()
    }

    override suspend fun getToken(): String? {
        val sharedPreferences = context.getSharedPreferences("userData", Context.MODE_PRIVATE)
        return sharedPreferences.getString("token", "")
    }

}
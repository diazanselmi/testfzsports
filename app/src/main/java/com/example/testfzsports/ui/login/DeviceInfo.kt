package com.example.testfzsports.ui.login

data class DeviceInfo(
    val name: String,
    val appVersion: String,
    val width: String,
    val height: String,
    val model: String,
    val platform: String,
    val deviceId: String
)

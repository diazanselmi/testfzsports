package com.example.testfzsports.ui.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.testfzsports.data.repository.JsonBodyGame
import com.example.testfzsports.databinding.ItemGamesBinding

class GroupBViewHolder(view: View): RecyclerView.ViewHolder(view) {
    private val binding = ItemGamesBinding.bind(view)
    fun bind(item: JsonBodyGame.Items) {
        binding.team1.text = item.homeTeam.name
        binding.team2.text = item.awayTeam.name
        binding.tvGameDate.text = item.startDate
        binding.tvStadium.text = item.matchDay.name.original
        binding.tvGameStatus.text = item.eventStatus.name.original
        binding.tvScoreTeam1.text = item.homeScore.toString()
        binding.tvScoreTeam2.text = item.awayScore.toString()

    }
}
package com.example.testfzsports.ui.main

import com.example.testfzsports.data.repository.datasource.RemoteRepository
import com.example.testfzsports.data.repository.datasource.SaveUserDataRepository
import com.example.testfzsports.usecases.LocalRequests
import com.example.testfzsports.usecases.RemoteRequests
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class ActivitiesModule {
    @Provides
    fun remoteRequestProvider(remoteRepository: RemoteRepository): RemoteRequests {
        return RemoteRequests(remoteRepository)
    }

    @Provides
    fun localRequestProvider(saveUserDataRepository: SaveUserDataRepository): LocalRequests {
        return LocalRequests(saveUserDataRepository)
    }
}
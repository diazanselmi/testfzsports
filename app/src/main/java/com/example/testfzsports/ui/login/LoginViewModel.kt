package com.example.testfzsports.ui.login

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testfzsports.R
import com.example.testfzsports.data.repository.datasource.RemoteRepository
import com.example.testfzsports.ui.data.datasource.ServerDataSource
import com.example.testfzsports.usecases.LocalRequests
import com.example.testfzsports.usecases.RemoteRequests
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val remoteRequests: RemoteRequests,
    private val localRequests: LocalRequests   ): ViewModel() {


    private val _loginForm = MutableLiveData<LoginFormState>()
        val loginForm : LiveData<LoginFormState> get() = _loginForm

    private val _deviceInfo = MutableLiveData<String>()
        //val deviceInfo : LiveData<DeviceInfo> get() = _deviceInfo

    private val _isLoginSuccessful = MutableLiveData<Bundle>()
        val isLoginSuccessful : LiveData<Bundle> get() = _isLoginSuccessful

    private var _email = String()

    private var _password = String()


    //useCase to login remote server
    private fun login(deviceInfo: String) {
      viewModelScope.launch {
        val bundle = remoteRequests.invokeLogin(deviceInfo)
          if   (bundle.getBoolean("isSuccessful")) {
              _isLoginSuccessful.value = bundle
              bundle.getString("token")?.let { saveUserData(_email, _password, it) }
          } else {
              _loginForm.value = LoginFormState(
                  isDataValid = false,
                  invalidFieldError = "${bundle.getString("message")}"
              )
          }
      }
    }

    //useCase to local store user info
    private fun saveUserData(email: String, password: String, token: String) {
        viewModelScope.launch {
            localRequests.invokeSaveUserData(email, password, token)
        }
    }

    //set device info to _deviceInfo val
    fun setDeviceInfo(deviceInfo: String) {
        _deviceInfo.value = deviceInfo
    }


    // A placeholder username validation check public function
    fun loginDataChanged(email: String, password: String) {
        if (areEditTextEmpty(email, password)) {

            _loginForm.value = LoginFormState(invalidFieldError = R.string.empty_edit_text.toString(),
                isDataValid = false)
        } else {
            if (isEmailValid(email)) {
                _loginForm.value = LoginFormState(isDataValid = true)
                _deviceInfo.value?.let { login(it)
                                          _email = email
                                          _password =  password }
            } else {
                _loginForm.value = LoginFormState(invalidFieldError = R.string.invalid_email.toString(),
                    isDataValid = false)
            }
        }
    }

    // A placeholder username validation check
    private fun isEmailValid(email: String): Boolean {
        return email.contains('@') && email.contains(".")
    }

    // A placeholder nullOrEmpty validation check
    private fun areEditTextEmpty(userName: String, email: String): Boolean {
        return userName.isEmpty() || email.isEmpty()
    }
}
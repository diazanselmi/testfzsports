package com.example.testfzsports.ui.login

data class LoginFormState(
    var invalidFieldError: String  = "",
    var isDataValid: Boolean = false
)
package com.example.testfzsports.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.testfzsports.R
import com.example.testfzsports.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "token"
private const val ARG_PARAM2 = "id"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class DetailFragment : Fragment() {

    private val viewModel: DetailViewModel by viewModels()
    private var _binding : FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private var token: String? = null
    private var id: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getString(ARG_PARAM2)
            token = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.progressBarDetail.visibility = View.VISIBLE
        id?.let { viewModel.getGameDetail(it) }
        viewModel.gameDetail.observe(this) {
            binding.progressBarDetail.visibility = View.GONE
            binding.tvHomeTeamDetail.text = it.data.homeTeam.name
            binding.tvAwayTeamDetail.text = it.data.awayTeam.name
            binding.tvHomeTeamScoreDetail.text = it.data.homeScore.toString()
            binding.tvAwayTeamScoreDetail.text = it.data.awayScore.toString()
            binding.tvLocationDetail.text = it.data.location.original
            binding.tvHomeTeamGoals.text = it.data.stats[0].homeValue.toString()
            binding.tvAwayTeamGoals.text = it.data.stats[0].awayValue.toString()
            binding.tvHomeTeamPossession.text = it.data.stats[1].homeValue.toString()
            binding.tvAwayTeamPossession.text = it.data.stats[1].awayValue.toString()
            binding.tvHomeTeamOffsides.text = it.data.stats[5].homeValue.toString()
            binding.tvAwayTeamOffsides.text = it.data.stats[5].awayValue.toString()
            binding.tvHomeTeamCorners.text = it.data.stats[6].homeValue.toString()
            binding.tvAwayTeamCorners.text = it.data.stats[6].awayValue.toString()
            binding.tvHomeTeamYellowCards.text = it.data.stats[7].homeValue.toString()
            binding.tvAwayTeamYellowCards.text = it.data.stats[7].awayValue.toString()
            binding.tvHomeTeamRedCards.text = it.data.stats[8].homeValue.toString()
            binding.tvAwayTeamRedCards.text = it.data.stats[8].awayValue.toString()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DetailFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
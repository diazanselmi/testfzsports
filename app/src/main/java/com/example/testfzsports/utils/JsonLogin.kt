package com.example.testfzsports.utils

private val bodyLogin = "{\n" +
        "\"user\": {\n" +
        "\"profile\": {\n" +
        "\"language\": \"es\"\n" +
        "}\n" +
        "},\n" +
        "\"device\": {\n" +
        "\"deviceId\": \"1234567\",\n" +
        "\"name\": \"MyPhone\",\n" +
        "\"version\": \"4.4.4\",\n" +
        "\"width\": \"640\",\n" +
        "\"heigth\": \"960\",\n" +
        "\"model\": \"Awesome Model 6\",\n" +
        "\"platform\": \"android\"\n" +
        "},\n" +
        "\"app\": {\n" +
        "\"version\": \"1.0.0\"\n" +
        "}\n" +
        "}"

fun bodyJsonLogin(): String {
    return bodyLogin
}
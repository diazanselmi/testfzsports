package com.example.testfzsports.ui.login

import android.os.Bundle
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.testfzsports.usecases.LocalRequests
import com.example.testfzsports.usecases.RemoteRequests
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class LoginViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val dispatcher = TestCoroutineDispatcher()

    @Mock
    lateinit var remoteRequests: RemoteRequests

    @Mock
    lateinit var localRequests: LocalRequests

    @Before
    fun setUp() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }


    @Test
    fun `email format checking`()  {
        val fakeLoginFormState = LoginFormState("2131755061", isDataValid = false)
        val vm = LoginViewModel(remoteRequests, localRequests)
        vm.loginDataChanged("sdjnsdljfksdm.com", "ldfjnnv")
        assertEquals(vm.loginForm.value,fakeLoginFormState)
    }

    @Test
    fun `empty editText checking`()  {
        val fakeLoginFormState = LoginFormState("2131755051", isDataValid = false)
        val vm = LoginViewModel(remoteRequests, localRequests)
        val emptyString = ""
        vm.loginDataChanged("sdjnsdljfksdm.com", emptyString)
        assertEquals(vm.loginForm.value,fakeLoginFormState)
    }
}